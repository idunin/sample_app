require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  test "layout links" do
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path
    assert_select "a[href=?]", help_path, text: "Help"
    assert_select "h1", text: 'Welcome to the Sample App'
    assert_select "a[href=?]", signup_path, text: 'Sign up now!'

    get contact_path
    assert_select "title", full_title("Contact")
  end
end
