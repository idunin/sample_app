require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  test "invalid signup information" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, params: { user: { name:  "",
                                         email: "user@invalid",
                                         password:              "foo",
                                         password_confirmation: "bar" } }
    end
    assert_template 'users/new'
    assert_select 'div#error_explanation'
    assert_select 'div.field_with_errors'
  end

  def setup
    ActionMailer::Base.deliveries.clear
  end


  test "valid signup information with account activation" do
    user_name = "Example User"

    get signup_path
    assert_difference 'User.count', 1 do
      post users_path, params: { user: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    end
    follow_redirect!
    #TODO: uncomment and fix
    # assert_template 'users/show'
    #assert_select 'div.alert.alert-success', 'Welcome to the Sample App!'
    #assert_select 'h1', user_name
    # assert is_logged_in?
  end
end
